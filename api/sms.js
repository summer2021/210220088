const Router = require('koa-router');
const axios = require('axios');
const router = new Router();
router
    .get('/sms/', (ctx, next) => {
        ctx.body = 'Hello World! Hello 5gSms!';
    })
    .get('/sms/notifyPath', (ctx, next) => {
        let echostr=ctx.request.header.echostr
        console.log(echostr)
        ctx.body = {
            msg:'here is test',
            appId:'28871d8c83954bc78424ffcbff80285c',
            echoStr:echostr,
            query:ctx.query,
            queryStr:ctx.querystring,
        }
        ctx.set("appId","28871d8c83954bc78424ffcbff80285c");
        ctx.set("echoStr",echostr)
    })
    .post('/sms/messageNotification/sip:20210401@botplatform.rcs.chinaunicom.cn/messages', (ctx, next) => {
        let messageId=ctx.request.body.messageId
        let conversationId=ctx.request.body.conversationId
        let contributionId=ctx.request.body.contributionId
        console.log(contributionId)
        let message=ctx.request.body
        console.log(message)
        if(ctx.request.body.messageList[0].contentType=="text/plain"){
            console.log(ctx.request.body.messageList[0].contentText)
        }
        ctx.body = {
            msg:'here is uplink message',
            contributionId,
            conversationId,
            messageId,
        }
    })
    .post('/sms/users', (ctx, next) => {
        ctx.body = 'here is users';
    })
    .all('/sms/users/:id', (ctx, next) => {
        // ...
    });
    

module.exports = router
